package lib.common;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

	private static Properties properties = new Properties();

	static {
		try {
			FileReader reader = new FileReader("src\\main\\resources\\application.properties");
			properties.load(reader);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	public static String get(String nameProperty) {
		return (String) properties.get(nameProperty);
	}

	private PropertiesReader() {
	}
}
