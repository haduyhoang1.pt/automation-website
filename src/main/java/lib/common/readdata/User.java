package lib.common.readdata;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class User {

	private String userName;

	private String password;

	private String comment;

}
