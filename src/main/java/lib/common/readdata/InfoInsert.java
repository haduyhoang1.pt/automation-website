package lib.common.readdata;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class InfoInsert {

	private List<User> users;

	private String urlPage;

}
