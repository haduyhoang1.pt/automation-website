package lib.common.readdata;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelUtils {

	public static Object getDataFromPath(String path) {
		FileInputStream inputStream = null;
		XSSFWorkbook workbook = null;
		try {
			inputStream = new FileInputStream(new File(path));
			workbook = new XSSFWorkbook(inputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		XSSFSheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();

//		hàng 1 cột 3
		Row row = rowIterator.next();
		Iterator<Cell> cellIterator = row.cellIterator();
		cellIterator.next();
		cellIterator.next();
		Cell cell = cellIterator.next();
		String urlPage = cell.getStringCellValue();

//		hàng 2, 3, 4, 5
		rowIterator.next();

		List<User> accountsFacebook = new ArrayList<>();

		while (rowIterator.hasNext()) {
			row = rowIterator.next();
			cellIterator = row.cellIterator();
			accountsFacebook.add(
					User.builder()
							.userName(cellIterator.next().getStringCellValue())
							.password(cellIterator.next().getStringCellValue())
							.comment(cellIterator.next().getStringCellValue())
							.build());
		}

		return InfoInsert.builder()
				.users(accountsFacebook)
				.urlPage(urlPage)
				.build();

//		while (rowIterator.hasNext()) {
//			Row row = rowIterator.next();
//			Iterator<Cell> cellIterator = row.cellIterator();
//
//			while (cellIterator.hasNext()) {
//				Cell cell = cellIterator.next();
//
//				// Đổi thành getCellType() nếu sử dụng POI 4.x
//				CellType cellType = cell.getCellType();
//
//				switch (cellType) {
//					case _NONE:
//						System.out.print("");
//						System.out.print("\t");
//						break;
//					case BOOLEAN:
//						System.out.print(cell.getBooleanCellValue());
//						System.out.print("\t");
//						break;
//					case BLANK:
//						System.out.print("");
//						System.out.print("\t");
//						break;
//					case FORMULA:
//						// Công thức
//						System.out.print(cell.getCellFormula());
//						System.out.print("\t");
//						FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
//						// In ra giá trị từ công thức
//						System.out.print(evaluator.evaluate(cell).getNumberValue());
//						break;
//					case NUMERIC:
//						System.out.print(cell.getNumericCellValue());
//						System.out.print("\t");
//						break;
//					case STRING:
//						System.out.print(cell.getStringCellValue());
//						System.out.print("\t");
//						break;
//					case ERROR:
//						System.out.print("!");
//						System.out.print("\t");
//						break;
//				}
//
//			}
//		}
	}

}
