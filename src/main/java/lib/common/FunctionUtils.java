package lib.common;

import lombok.extern.log4j.Log4j2;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@Log4j2
public class FunctionUtils {

    // random int in [n1, n2]
    public static int randomInt(int n1, int n2) {
        Random random = new Random();
        int less = Math.min(n1, n2);
        int bigger = Math.max(n1, n2);
        int duration = bigger - less;
        return less + random.nextInt(duration);
    }

    public static void sleepSecondRandom(int beginDuration, int endDuration) {
        try {
            int timeSleep = randomInt(beginDuration, endDuration);
            log.info("sleep in " + timeSleep + " second.");
            Thread.sleep(TimeUnit.SECONDS.toMillis(timeSleep));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void sleepInSecond(long second) {
        try {
            log.info("sleep in " + second + " second.");
            Thread.sleep(TimeUnit.SECONDS.toMillis(second));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

//    public static List<String> convertRowToData(WebElement rowTitles, By identifyTagItem) {
//        List<WebElement> titleList = rowTitles.findElements(identifyTagItem);
//        if (CollectionUtils.isEmpty(titleList)) {
//            return Collections.emptyList();
//        }
//        return titleList.stream().map(WebElement::getText).collect(Collectors.toList());
//    }
//
//    public static HashMap<String, String> convertRowToData(List<String> titleList, WebElement row, By identifyTagItem) {
//        if (titleList == null) {
//            titleList = new ArrayList<>();
//        }
//
//        if (titleList.stream().distinct().count() != titleList.size()) {
//            throw new RuntimeException("List title phải có các phần tử không trùng nhau vì dùng làm key cho Map");
//        }
//
//        List<String> item = row.findElements(identifyTagItem).stream().map(WebElement::getText).collect(Collectors.toList());
//        HashMap<String, String> result = new HashMap<>();
//
//        for (int i = 0; i < item.size(); i++) {
//            String title;
//
//            if (i < titleList.size()) {
//                title = titleList.get(i);
//            } else {
//                title = "empty " + i;
//            }
//
//            result.put(title, item.get(i));
//        }
//
//        return result;
//    }
//
//    public static List<HashMap<String, String>> convertTableToData(List<String> titleList, WebElement table, By identifyTagRow, By identifyTagItem) {
//        List<WebElement> rows = table.findElements(identifyTagRow);
//
//        return rows.stream().map(
//                row -> convertRowToData(titleList, row, identifyTagItem)
//        ).collect(Collectors.toList());
//    }
//
//    public static List<HashMap<String, String>> convertTableToData(WebElement titleRow, By identifyTagTitleItem, WebElement table, By identifyTagRow, By identifyTagItem) {
//        List<String> titleList = convertRowToData(titleRow, identifyTagTitleItem);
//
//        List<WebElement> rows = table.findElements(identifyTagRow);
//
//        return rows.stream().map(
//                row -> convertRowToData(titleList, row, identifyTagItem)
//        ).collect(Collectors.toList());
//    }

}
