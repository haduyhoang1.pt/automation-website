package lib.website.element;

import lib.website.Screen;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class OptionGroup {

    private Button buttonOpen;

    private List<Option> optionList;

    public OptionGroup(Button buttonOpen, Element elementParentOfListOption, By cssSelectorOfOption) {
        this.buttonOpen = buttonOpen;

        elementParentOfListOption.reload();
        optionList = elementParentOfListOption.element.findElements(cssSelectorOfOption)
                .stream()
                .map(Option::new)
                .collect(Collectors.toList());
    }

    public void defineOption(By option, Element elementParent) {

    }

    public static class Option extends Button {

        public Option(WebElement webElement) {
            super(webElement);
        }

        public Option(By identify, Screen screen) {
            super(identify, screen);
        }

        public Option(By identify, Element elementParent) {
            super(identify, elementParent);
        }
    }
}
