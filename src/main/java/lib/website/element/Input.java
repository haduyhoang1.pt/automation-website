package lib.website.element;

import org.openqa.selenium.By;
import lib.website.Screen;

public class Input extends Element {

	public Input(By identify, Screen screen) {
		super(identify, screen);
	}

	public Input(By identify, Element elementParent) {
		super(identify, elementParent);
	}

	public void sendKeys(String text) {
		reload();
		waitElementToBeClickable();
		element.click();
		element.clear();
		element.sendKeys(text);
	}

	public void sendKeysThenSleepRandom(String text) {
		sendKeys(text);
		sleepSecondRandom();
	}

	public void sendKeysThenSleepRandom(String text, int beginDuration, int endDuration) {
		sendKeys(text);
		sleepSecondRandom(beginDuration, endDuration);
	}
}
