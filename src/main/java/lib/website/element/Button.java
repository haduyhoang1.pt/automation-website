package lib.website.element;

import org.openqa.selenium.By;
import lib.website.Screen;
import org.openqa.selenium.WebElement;

public class Button extends Element {

	public Button(WebElement webElement) {
		super(webElement);
	}

	public Button(By identify, Screen screen) {
		super(identify, screen);
	}

	public Button(By identify, Element elementParent) {
		super(identify, elementParent);
	}

	public void click() {
		reload();
		waitElementToBeClickable();
		element.click();
	}

	public void clickThenSleepRandom() {
		click();
		sleepSecondRandom();
	}


}
