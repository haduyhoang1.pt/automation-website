package lib.website.element;

import com.sun.istack.NotNull;
import lib.common.FunctionUtils;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import lib.website.Screen;

@Getter
public abstract class Element {

    protected By identify;

    protected WebElement element;

    protected Screen screen;

    protected Element elementParent;

    public Element(@NotNull WebElement webElement) {
        this.element = webElement;

        initChildElement();
        validateCreate();
        reload();
    }

    public Element(@NotNull By identify, @NotNull Screen screen) {
        this.identify = identify;
        this.screen = screen;

        initChildElement();
        validateCreate();
        reload();
    }

    public Element(@NotNull By identify, @NotNull Element elementParent) {
        this.identify = identify;
        this.elementParent = elementParent;

        initChildElement();
        validateCreate();
        reload();
    }

    protected void validateCreate() {
    }

    protected void initChildElement() {
    }

    public void reload() {
        if (screen != null) {
            this.element = screen.getWebsite().findElement(identify);
        } else if (elementParent != null && elementParent.getElement() != null) {
            this.element = elementParent.getElement().findElement(identify);
        }
    }

    public boolean isDisplayed() {
        return this.element.isDisplayed();
    }

    public boolean isExistAndDisplayed() {
        return this.element != null && isDisplayed();
    }

    public void waitElementToBeClickable() {
        if (screen != null) {
            this.screen.getWebsite().getWait().until(ExpectedConditions.elementToBeClickable(this.element));
        } else if (elementParent != null && elementParent.screen != null) {
            this.elementParent.screen.getWebsite().getWait().until(ExpectedConditions.elementToBeClickable(this.element));
        }
    }

    public void sleepSecondRandom() {
        FunctionUtils.sleepSecondRandom(screen.getWebsite().getBeginRandom(), screen.getWebsite().getEndRandom());
    }

    public void sleepSecondRandom(int beginDuration, int endDuration) {
        FunctionUtils.sleepSecondRandom(beginDuration, endDuration);
    }
}
