package lib.website;

import com.sun.istack.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

@Getter
@Log4j2
public abstract class Screen {

	protected Website website;

	@Setter(AccessLevel.PUBLIC)
	protected String urlRelative;

	protected String name;

	public Screen(@NotNull Website website, String name) {
		this.website = website;
		this.name = name;

		initElement();
	}

	protected abstract void initElement();

	public void comeByUrlAndSleepRandom() {
		if (StringUtils.isBlank(urlRelative)) {
			log.warn("urlRelative is blank! Not predict.");
			return;
		}
		website.comeUrlAndSleepRandom(website.getRootUrl() + urlRelative);
	}

	public void comeByUrlAndSleepRandom(int beginRandom, int endRandom) {
		if (StringUtils.isBlank(urlRelative)) {
			log.warn("urlRelative is blank! Not predict.");
			return;
		}
		website.comeUrlAndSleepRandom(website.getRootUrl() + urlRelative, beginRandom, endRandom);
	}

}
