package lib.website;

import com.sun.istack.NotNull;
import lib.common.FunctionUtils;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Data
public abstract class Website {

	protected static final int timeOutInSeconds = 20;

	protected int beginRandom = 3;

	protected int endRandom = 6;

	@Setter(value = AccessLevel.PRIVATE)
	private WebDriver driver;

	protected String rootUrl;

	@Setter(value = AccessLevel.PRIVATE)
	protected WebDriverWait wait;

	// screen đang được hiển thị trên màn hình
	@Setter(value = AccessLevel.PRIVATE)
	protected Screen current;

	@Setter(value = AccessLevel.PRIVATE)
	protected List<Screen> allScreen = new ArrayList<>();

	// waiting screen load

	public Website(@NotNull WebDriver driver, String rootUrl) {
		this.driver = driver;
		this.rootUrl = rootUrl;
		wait = new WebDriverWait(driver, Duration.ofSeconds(timeOutInSeconds));
		initScreen();
	}

	public Website(@NotNull WebDriver driver, String rootUrl, int secondsWaitingLoadScreenComplete) {
		this.driver = driver;
		this.rootUrl = rootUrl;
		wait = new WebDriverWait(driver, Duration.ofSeconds(secondsWaitingLoadScreenComplete));
		initScreen();
	}

	public abstract void initScreen();

	public void addScreen(@NotNull Screen screen) {
		allScreen.add(screen);
	}

	public void comeUrlAndSleepRandom(String url) {
		comeUrl(url);
		FunctionUtils.sleepSecondRandom(beginRandom, endRandom);
	}

	public void comeUrlAndSleepRandom(String url, int beginRandom, int endRandom) {
		comeUrl(url);
		FunctionUtils.sleepSecondRandom(beginRandom, endRandom);
	}

	public void comeUrl(String url) {
		waitScreenLoadComplete();
		driver.get(url);
		waitScreenLoadComplete();
	}

	public WebElement findElement(By elementInput) {
		try {
			return driver.findElement(elementInput);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void waitScreenLoadComplete() {
		wait.until(driver1 -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
	}

	public void clearAllStorageAndCache() {
		driver.manage().deleteAllCookies();
		WebStorage webStorage = (WebStorage) driver;
		webStorage.getSessionStorage().clear();
		webStorage.getLocalStorage().clear();
	}

}
