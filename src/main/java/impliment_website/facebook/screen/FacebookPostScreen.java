package impliment_website.facebook.screen;

import lib.website.Screen;
import org.openqa.selenium.By;
import lib.website.Website;
import lib.website.element.Button;
import lib.website.element.Input;

public class FacebookPostScreen extends Screen {

	private Input textBoxComment;

	private Button likeButton;

	public FacebookPostScreen(Website website, String name) {
		super(website, name);
	}

	@Override
	protected void initElement() {
		textBoxComment = new Input(By.cssSelector("div[role='textbox'][contenteditable='true'][aria-label='Viết bình luận']"), this);
		likeButton = new Button(By.cssSelector("i[data-visualcompletion='css-img'][class='hu5pjgll m6k467ps']"), this);
	}

	public void comment(String comment, int beginDuration, int endDuration) {
		textBoxComment.sendKeysThenSleepRandom(comment, beginDuration, endDuration);
	}

	public void like() {
		likeButton.click();
	}

}
