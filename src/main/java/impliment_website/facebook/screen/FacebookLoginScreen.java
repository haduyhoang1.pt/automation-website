package impliment_website.facebook.screen;

import com.sun.istack.NotNull;
import lib.website.Screen;
import org.openqa.selenium.By;
import lib.website.Website;
import lib.website.element.Button;
import lib.website.element.Input;

public class FacebookLoginScreen extends Screen {

	private Input inputUserName;

	private Input inputPassword;

	private Button buttonLogin;

	public FacebookLoginScreen(@NotNull Website website, String name) {
		super(website, name);
		this.urlRelative = "/";
	}

	@Override
	protected void initElement() {
		inputUserName = new Input(By.id("email"), this);
		inputPassword = new Input(By.id("pass"), this);
		buttonLogin = new Button(By.cssSelector("button[name='login'][type='submit']"), this);
	}

	public Boolean login(String userName, String password) {

		inputUserName.sendKeysThenSleepRandom(userName);
		inputPassword.sendKeysThenSleepRandom(password);
		buttonLogin.clickThenSleepRandom();

		return true;
	}

}
