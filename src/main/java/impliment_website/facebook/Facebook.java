package impliment_website.facebook;

import lib.website.Website;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import impliment_website.facebook.screen.FacebookLoginScreen;
import impliment_website.facebook.screen.FacebookPostScreen;

@Getter
public class Facebook extends Website {

	FacebookLoginScreen loginScreen;

	FacebookPostScreen postScreen;

	public Facebook(WebDriver driver) {
		super(driver, "https://www.facebook.com");
	}

	public Facebook(WebDriver driver, int secondsWaitingLoadScreenComplete) {
		super(driver, "https://www.facebook.com", secondsWaitingLoadScreenComplete);
	}

	public void initScreen() {
		loginScreen = new FacebookLoginScreen(this, "màn hình đăng nhập");
		postScreen = new FacebookPostScreen(this, "một bài đăng bất kỳ");
	}
}
