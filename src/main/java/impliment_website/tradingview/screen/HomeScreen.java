package impliment_website.tradingview.screen;

import lib.website.Screen;
import lib.website.Website;
import lib.website.element.Button;
import lombok.Getter;
import org.openqa.selenium.By;

@Getter
public class HomeScreen extends Screen {

    private Button menuButton;

    public HomeScreen(Website website, String name) {
        super(website, name);
        this.urlRelative = "/";
    }

    @Override
    protected void initElement() {
        menuButton = new Button(By.cssSelector("button[aria-label='Open menu']"), this);
    }
}
