package impliment_website.tradingview.screen;

import impliment_website.tradingview.element.OptionCountryGroup;
import lib.website.Screen;
import lib.website.Website;
import lib.website.element.Button;
import lib.website.element.Element;
import lib.website.element.Input;
import lombok.Getter;
import org.openqa.selenium.By;

public class ChartScreen extends Screen {

    private Button buttonOpenListStock;

    private FilterStock filterStock;

    public ChartScreen(Website website, String name) {
        super(website, name);
        this.urlRelative = "/chart/";
    }

    @Override
    protected void initElement() {
        buttonOpenListStock = new Button(By.cssSelector("button[id='header-toolbar-symbol-search']"), this);
        filterStock = new FilterStock(this);
    }

    public void changeChartForStock(String codeStock) {
        if (!filterStock.isExistAndDisplayed()) {
            buttonOpenListStock.clickThenSleepRandom();
        }

        filterStock.reload();

        if (!filterStock.isExistAndDisplayed()) {
            throw new RuntimeException("Không thể mở " + FilterStock.NAME);
        }

        filterStock.filterStock.clickThenSleepRandom();

        filterStock.openFilterCountry.click();
        filterStock.inputSearch.sendKeys("vietnam");
        filterStock.itemCountry.click();
        filterStock.inputSearch.sendKeys(codeStock + "\n");

    }

    @Getter
    public static class FilterStock extends Element {

        private Button buttonClose;

        // dùng để search country và cả stock code
        private Input inputSearch;

        private Button filterStock;

        private Button openFilterCountry;

        private OptionCountryGroup optionCountryGroup;

        private Button itemCountry;

        private static final String NAME = "Danh sách cổ phiếu và tìm kiếm";

        private static final By CSS_SELECT = By.cssSelector("div[data-name='symbol-search-items-dialog'][data-dialog-name='Symbol Search']");

        private FilterStock(Screen screen) {
            super(CSS_SELECT, screen);
        }

        public FilterStock(Element elementParent) {
            super(CSS_SELECT, elementParent);
        }

        protected void initChildElement() {
            buttonClose = new Button(By.cssSelector("button[data-name='close'][type='button']"), this);
            inputSearch = new Input(By.cssSelector("input[data-role='search']"), this);
            filterStock = new Button(By.cssSelector("span[id='stocks']"), this);
            openFilterCountry = new Button(By.cssSelector("button[data-name='sources-button']"), this);
            itemCountry = new Button(By.cssSelector("div[class='wrap-IxKZEhmO']"), this);
        }

        @Override
        public void reload() {
            super.reload();
            buttonClose.reload();
            inputSearch.reload();
            filterStock.reload();
            openFilterCountry.reload();
            itemCountry.reload();
        }
    }
}
