package impliment_website.tradingview.element;

import lib.website.element.Button;
import lib.website.element.Element;
import lib.website.element.OptionGroup;
import org.openqa.selenium.By;

public class OptionCountryGroup extends OptionGroup {
    public OptionCountryGroup(Button buttonOpen, Element elementParentOfListOption, By cssSelectorOfOption) {
        super(buttonOpen, elementParentOfListOption, cssSelectorOfOption);
    }
}
