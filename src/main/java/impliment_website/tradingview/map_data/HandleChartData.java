package impliment_website.tradingview.map_data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HandleChartData {

    /**
     * always return string json of List<Map>
     * @param message
     * @return
     */
    public static List<Map<String, Object>> convertReceiveToListMap(String message) {
        String jsonMessage = convertReceiveToJson(message);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(jsonMessage, List.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String convertReceiveToJson(String message) {
        final String separationRegex = "~m~\\d+~m~";
        final String checkConnectRegex = "~h~(\\d)";

        message = message.trim().replaceAll("^" + separationRegex, "");
        Matcher matcher;

        Pattern separation = Pattern.compile(separationRegex);
        matcher = separation.matcher(message);
        message = matcher.replaceAll(",");

        Pattern checkConnectMatch = Pattern.compile(checkConnectRegex);
        matcher = checkConnectMatch.matcher(message);
        message = matcher.replaceAll("{\"h\": $1}");

        return String.format("[%s]", message);
    }

}
