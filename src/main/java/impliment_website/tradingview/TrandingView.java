package impliment_website.tradingview;

import impliment_website.tradingview.screen.ChartScreen;
import impliment_website.tradingview.screen.HomeScreen;
import lib.website.Website;
import lombok.Getter;
import org.openqa.selenium.WebDriver;

@Getter
public class TrandingView extends Website {

    private HomeScreen homeScreen;

    private ChartScreen chartScreen;

    public TrandingView(WebDriver driver) {
        super(driver, "https://www.tradingview.com");
    }

    public TrandingView(WebDriver driver, int secondsWaitingLoadScreenComplete) {
        super(driver, "https://www.tradingview.com", secondsWaitingLoadScreenComplete);
    }

    @Override
    public void initScreen() {
        homeScreen = new HomeScreen(this, "Trang chủ");
        chartScreen = new ChartScreen(this, "Biểu đồ kĩ thuật");
    }
}
