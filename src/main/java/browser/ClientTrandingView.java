package browser;

import impliment_website.tradingview.TrandingView;
import impliment_website.tradingview.map_data.HandleChartData;
import lib.common.FunctionUtils;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v114.network.Network;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

public class ClientTrandingView {

    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.silentOutput", "true");
        java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);

        ChromeDriverSimple chromeDriverSimple = ChromeDriverSimple.Builder()
                //add key and value to map as follows to switch off browser notification
                //Pass the argument 1 to allow and 2 to block
                .putPref("profile.default_content_setting_values.notifications", 2)
                .useDevTools(true)
                .build();

        DevTools devTools = chromeDriverSimple.getDevTools();

        devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
        devTools.addListener(Network.webSocketFrameReceived(),
                request -> handleMessageReceive(request.getResponse().getPayloadData())
        );

        TrandingView trandingview = new TrandingView(chromeDriverSimple.getDriver());

        trandingview.getHomeScreen().comeByUrlAndSleepRandom();
        trandingview.getChartScreen().comeByUrlAndSleepRandom();
        trandingview.getChartScreen().changeChartForStock("SGP");

//        FunctionUtils.sleepInSecond(TimeUnit.MINUTES.toSeconds(5));
        FunctionUtils.sleepInSecond(30);

        chromeDriverSimple.getDriver().quit();
    }

    public static void handleMessageReceive(String messageOrigin) {
        List<Map<String, Object>> value = HandleChartData.convertReceiveToListMap(messageOrigin);

        Map<String, Object> objectMap = value.get(0);
        String m = (String) objectMap.get("m");

        // nếu là thông tin chart
        if ("timescale_update".equalsIgnoreCase(m)) {
            List value2 = (List) objectMap.get("p");
            Map<String, Object> objectMap2 = (Map<String, Object>) value2.get(1);
            Map<String, Object> objectMap3 = (Map<String, Object>) objectMap2.get("sds_1");
            List chartData = (List) objectMap3.get("s");

            System.out.println("CHART DATA: " + chartData);
        }
    }
}
