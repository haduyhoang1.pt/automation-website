package browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import lib.common.PropertiesReader;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

@Getter
public class ChromeDriverSimple {

    private static String chromeVersion = PropertiesReader.get("brower.chrome.version");
    private ChromeDriver driver;
    private DevTools devTools;

    private ChromeDriverSimple() {
    }

    public static Builder Builder() {
        return new Builder();
    }

    @Accessors(fluent = true)
    @Getter(AccessLevel.PRIVATE)
    @Setter
    public static class Builder {

        private Builder() {
        }

        private boolean useDevTools = false;

        @Setter(AccessLevel.PRIVATE)
        // prefs is yes or no for notify
        private Map<String, Object> prefs = new HashMap<>();

        private boolean cacheBrowserInfo = false;

        public Builder putPref(String key, Object value) {
            prefs.put(key, value);
            return this;
        }

        public ChromeDriverSimple build() {
            ChromeDriverSimple chromeDriverSimple = new ChromeDriverSimple();
            // set version browser local
            WebDriverManager.chromedriver().browserVersion(chromeVersion).setup();

            // disable log selenium
            System.setProperty("webdriver.chrome.silentOutput", "true");
            java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);

            // set option
            ChromeOptions options = new ChromeOptions();
            options.addArguments("enable-automation");
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-infobars");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--disable-browser-side-navigation");
            options.addArguments("--remote-allow-origins=*");

            if (prefs != null && prefs.size() != 0) {
                options.setExperimentalOption("prefs", prefs);
            }
            if (cacheBrowserInfo) {
                String currentDirectory = System.getProperty("user.dir");
                options.addArguments("user-data-dir=" + currentDirectory + "/../data_chrome");
            }

            chromeDriverSimple.driver = new ChromeDriver(options);

            if (useDevTools) {
                chromeDriverSimple.devTools = chromeDriverSimple.driver.getDevTools();
                chromeDriverSimple.devTools.createSession();
            }

            return chromeDriverSimple;
        }
    }
}
