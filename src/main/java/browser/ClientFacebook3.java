package browser;

import impliment_website.facebook.Facebook;
import impliment_website.facebook.screen.FacebookPostScreen;
import lib.common.FunctionUtils;
import lib.common.PropertiesReader;

import java.util.concurrent.TimeUnit;

public class ClientFacebook3 {

	public static void main(String[] args) {
		ChromeDriverSimple chromeDriverSimple = ChromeDriverSimple.Builder()
				//add key and value to map as follows to switch off browser notification
				//Pass the argument 1 to allow and 2 to block
				.putPref("profile.default_content_setting_values.notifications", 2)
				.build();

		Facebook facebook = new Facebook(chromeDriverSimple.getDriver());

		// login
		facebook.getLoginScreen().comeByUrlAndSleepRandom(6, 13);
		facebook.getLoginScreen().login(PropertiesReader.get("facebook.username"), PropertiesReader.get("facebook.password"));

		// comment post
		FacebookPostScreen postScreen = facebook.getPostScreen();
		postScreen.setUrlRelative("/Hutostem/videos/558104184860148/");
		postScreen.comeByUrlAndSleepRandom(7, 20);
		postScreen.comment("hôm nay trời đẹp quá\n", 7, 20);

		FunctionUtils.sleepInSecond(TimeUnit.MINUTES.toSeconds(5));

	}

}
